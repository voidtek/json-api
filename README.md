# JSON-API

**An API that load data from json.**

## API Usage

## Development

This API is based on [FastAPI](https://fastapi.tiangolo.com/).

### Docker

```
docker build -t json-api .
docker run -d --name example-api -p 5000:5000 json-api
docker run --name example-api -p 5000:5000 json-api
docker stop example-api
docker rm example-api
```

## Acknowledgements

Thanks to my family.

## License

This software is licensed under the terms of the [MIT License](./LICENSE).

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import random

from app.main import app
from fastapi.testclient import TestClient

client = TestClient(app)

with open("./data.json") as f:
    all_items = json.load(f)
no_of_items = len(all_items)


def check_item_in_file(item_json):
    if item_json["item"] in all_items:
        return True
    return False


# Base test
def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"item": "Yo momma"}


# Test for asserting random item is in the file
def test_get_message_text():
    response = client.get("/items")
    response_json = response.json()
    assert response.status_code == 200
    assert check_item_in_file(response_json)


# Test for asserting length of query count > 1
def test_query_count():
    random_count = random.randint(2, no_of_items)
    response = client.get(f"/items?count={random_count}")
    response_json = response.json()
    assert response.status_code == 200
    assert len(response_json) == random_count


# Test for asserting invalid query count
def test_invalid_query_count():
    random_count = random.randint(no_of_items, no_of_items * 2)
    response = client.get(f"/items?count={random_count}")
    assert response.status_code == 404


# Test for asserting correct item is indexed
def test_specific_index():
    random_index = random.randint(0, no_of_items - 1)
    response = client.get(f"/items/{random_index}")
    assert response.status_code == 200
    assert response.json()["item"] == all_items[random_index]


# Test for asserting index out of range
def test_index_out_of_range():
    random_index = random.randint(no_of_items, no_of_items * 2)
    response = client.get(f"/items/{random_index}")
    assert response.status_code == 404


# Test for asserting index out of range (negative)
def test_index_out_of_range_negative():
    random_index = -1 * random.randint(no_of_items, no_of_items * 2)
    response = client.get(f"/items/{random_index}")
    assert response.status_code == 404


# Test for asserting correct search results
def test_search():
    query = random.choice(random.choice(all_items).split())
    response = client.get(f"/search?query={query}")
    assert response.status_code == 200
    for search_result in response.json()["results"]:
        assert query in search_result.lower()

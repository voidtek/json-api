#!/usr/bin/env python
# -*- coding: utf-8 -*-
from json import load
from random import choice

from fastapi import FastAPI
from fastapi.exceptions import HTTPException
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.errors import RateLimitExceeded
from slowapi.middleware import SlowAPIMiddleware
from slowapi.util import get_remote_address

with open("./data.json") as f:
    json_data = load(f)

limiter = Limiter(
    key_func=get_remote_address,
    default_limits=["10/second"],
)
app = FastAPI()
app.state.limiter = limiter
app.add_exception_handler(
    RateLimitExceeded, _rate_limit_exceeded_handler
)
app.add_middleware(SlowAPIMiddleware)


@app.get("/")
async def info_api():
    return {
        "title": json_data["title"],
        "version": json_data["version"],
    }


@app.get("/random")
async def item_random():
    item = choice(json_data["items"])
    return {
        "content": item["text"],
        "tag": item["tag"],
        "link": item["url"],
        "background": item["opts"],
        "published": item["published"],
    }


@app.get("/id/{index}")
async def item_by_id(index: int):
    if index > len(json_data["items"]) or index < 0:
        raise HTTPException(
            status_code=404, detail="Index out of range"
        )
    item = json_data["items"][index]
    return {
        "content": item["text"],
        "tag": item["tag"],
        "link": item["url"],
        "background": item["opts"],
        "published": item["published"],
    }


@app.get("/search/{query}")
async def item_by_text(query: str):
    result = [
        item
        for item in json_data["items"]
        if query in item.lower().split()
    ]
    return {"results": result}
